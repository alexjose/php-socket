<?php
    
    $entryData = array(
        'topic' => 'topic-' . $_GET['topic'],
        'title'    => 'Post Title ' . random_int(1, 1000),
        'article'  => 'My Article',
        'when'     => time()
    );

    // $pdo->prepare("INSERT INTO blogs (title, article, category, published) VALUES (?, ?, ?, ?)")
    //     ->execute($entryData['title'], $entryData['article'], $entryData['category'], $entryData['when']);

    // This is our new stuff
    $context = new ZMQContext();
    $socket = $context->getSocket(ZMQ::SOCKET_PUSH, 'my pusher');
    $socket->connect("tcp://localhost:5555");

    $socket->send(json_encode($entryData));

    echo '<pre>';
    var_dump($entryData);