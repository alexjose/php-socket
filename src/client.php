<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Socker Client</title>
</head>
<body>
<script src="js/ab.js"></script>
<script>
    var topics = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    var topic = topics[Math.floor(Math.random() * topics.length)];
    console.log('Listening to Topic: ' + topic);
    var conn = new ab.Session('ws://localhost:8080',
        function() {
            conn.subscribe('topic-' + topic, function(topic, data) {
                // This is where you would add the new article to the DOM (beyond the scope of this tutorial)
                console.log('New article published to category "' + topic + '" : ' + data.title);
            });
        },
        function() {
            console.warn('WebSocket connection closed');
        },
        {'skipSubprotocolCheck': true}
    );
</script>
</body>
</html>